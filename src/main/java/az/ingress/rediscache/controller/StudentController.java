package az.ingress.rediscache.controller;

import az.ingress.rediscache.dto.StudentDto;
import az.ingress.rediscache.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class StudentController {

private final StudentService studentService;

    @GetMapping("/student/{id}")
    public StudentDto getStudentById(@PathVariable Integer id) {
        return studentService.findById(id);
    }

    @GetMapping("/student")
    public List<StudentDto> getStudents() {
        return studentService.findAll();
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable Integer id) {
        studentService.deleteById(id);
    }

    @PutMapping("/student/{id}")
    public StudentDto updateStudent(@PathVariable Integer id, @RequestBody StudentDto studentDto) {
        return studentService.updateById(id,studentDto);
    }
}
