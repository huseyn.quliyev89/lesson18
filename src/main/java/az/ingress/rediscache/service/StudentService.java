package az.ingress.rediscache.service;

import az.ingress.rediscache.dto.StudentDto;


import java.util.List;

public interface StudentService {
    StudentDto findById(Integer id);
    List<StudentDto> findAll();
    void deleteById(Integer id);
    StudentDto updateById(Integer id, StudentDto studentDto);




}
