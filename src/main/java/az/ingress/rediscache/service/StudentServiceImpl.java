package az.ingress.rediscache.service;

import az.ingress.rediscache.dto.StudentDto;
import az.ingress.rediscache.mapper.StudentMapper;
import az.ingress.rediscache.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import az.ingress.rediscache.model.Student;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

   
    @Override
    @Cacheable(value = "student", key = "#id")
    public StudentDto findById(Integer id) {
        Student studentDb = studentRepository.findById(id).orElseThrow(RuntimeException::new);
        return studentMapper.mapToStudentDto(studentDb);
    }

    @Override
    @Cacheable(value = "studentList")
    public List<StudentDto> findAll() {
        return studentMapper.mapToStudentDtoList(studentRepository.findAll());
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "studentList", allEntries = true),
            @CacheEvict(value = "student", key = "#id")})
    public void deleteById(Integer id) {
        studentRepository.findById(id).orElseThrow(RuntimeException::new);
        studentRepository.deleteById(id);
    }

    @Override

    @Caching(
            evict = {@CacheEvict(value = "studentList", allEntries = true)},
            put = {@CachePut(value = "student", key = "#id")})
    public StudentDto updateById(Integer id, StudentDto studentDto) {
        studentRepository.findById(id).orElseThrow(RuntimeException::new);
        Student student = studentMapper.mapToStudent(studentDto);
        student.setId(id);
        Student savedStudent = studentRepository.save(student);
        return studentMapper.mapToStudentDto(savedStudent);
    }
}
