package az.ingress.rediscache.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import az.ingress.rediscache.model.Student;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    String number;
    @JsonIgnoreProperties("phones")
    Student student;
}
