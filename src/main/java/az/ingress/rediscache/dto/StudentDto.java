package az.ingress.rediscache.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;
import az.ingress.rediscache.model.Phone;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    String name;
    String surname;
    Integer age;
    @JsonIgnoreProperties("student")
    List<PhoneDto> phones;
}
