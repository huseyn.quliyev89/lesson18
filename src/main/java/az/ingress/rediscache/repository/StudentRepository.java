package az.ingress.rediscache.repository;

import az.ingress.rediscache.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Integer> {
    //@Query(value = "select b from Student b left join fetch b.phones")
    @EntityGraph(attributePaths = {"phones"},type = EntityGraph.EntityGraphType.FETCH)
    List<Student> findAll();

    @EntityGraph(attributePaths = {"phones"},type = EntityGraph.EntityGraphType.FETCH)
    Optional<Student> findById(Integer id);
}
