package az.ingress.rediscache.repository;

import az.ingress.rediscache.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone,Integer> {
}
