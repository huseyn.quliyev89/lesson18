package az.ingress.rediscache.mapper;

import az.ingress.rediscache.dto.PhoneDto;
import az.ingress.rediscache.model.Phone;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PhoneMapper {
    PhoneDto mapToPhoneDto(Phone phone);
    Phone mapToPhone(PhoneDto phoneDto);
    List<PhoneDto> mapToPhoneDtoList(List<Phone> phoneList);
}
