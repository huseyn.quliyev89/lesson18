package az.ingress.rediscache.mapper;

import az.ingress.rediscache.dto.StudentDto;
import az.ingress.rediscache.model.Student;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface StudentMapper {
    StudentDto mapToStudentDto(Student student);
    Student mapToStudent(StudentDto studentDto);
    List<StudentDto> mapToStudentDtoList(List<Student> studentList);
}
